﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class SatisReport:ReportingBase
    {
        public override string ReportName
        {
            get { return "Satis Raporu"; }
        }

        public override string SqlQuery
        {
            get { return "select * from Satislar"; }
        }
    }
}
