﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            List<Kategori> kategoriler=new List<Kategori>();
            for (int i = 0; i < 20; i++)
            {
                kategoriler.Add(new Kategori()
                {
                    Ad=Guid.NewGuid().ToString("N").Substring(0,5)
                });
            }

            foreach (var kategori in kategoriler)
            {
                kategori.AltKategoriler = AltKategoriOlustur();
            }



            foreach (var kategori in kategoriler)
            {
                var node = treeView1.Nodes.Add(kategori.Ad);
                foreach (var altKategori in kategori.AltKategoriler)
                {
                    node.Nodes.Add(altKategori.Ad);
                }
            }

        }

        Random r=new Random();
        List<Kategori> AltKategoriOlustur()
        {
            int adet = r.Next(0, 10);
            var result = new List<Kategori>();

            for (int i = 0; i < adet; i++)
            {
                result.Add(new Kategori()
                {
                    Ad = Guid.NewGuid().ToString("N").Substring(0, 5)
                });
            }

            return result;
        } 

    }

    class Kategori
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public List<Kategori> AltKategoriler { get; set; }
        public Kategori UstKategori { get; set; }
    }
}
