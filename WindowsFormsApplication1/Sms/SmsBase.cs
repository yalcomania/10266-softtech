﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Sms
{
    public abstract class SmsBase
    {

        public abstract string CreateXml(string telNo, string mesaj);
        public abstract string PostUrl { get; }

        public virtual string SmsGonder(string telefonNo, string mesaj)
        {
            //var request = (HttpWebRequest)WebRequest.Create("http://www.postaguvercini.com/smssend");
            var request = (HttpWebRequest)WebRequest.Create(PostUrl);

            //var postData = "<postaguvercini><sms>" 
            //        + telefonNo + "</sms><mesaj>"
            //        +mesaj+"</mesaj></postaguvercini>";

            var postData = CreateXml(telefonNo, mesaj);
            
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }


    }
}
