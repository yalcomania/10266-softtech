﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class frmTom : Form
    {
        public frmTom()
        {
            InitializeComponent();
        }

        Tom tom = new Tom();
        Tom tom2;
        

        private void frmTom_Load(object sender, EventArgs e)
        {
            this.Text = UserInfo.CurrentUser.Ad + " (" + UserInfo.CurrentUser.Id + ")";
            
        }

        private void btnSonDurum_Click(object sender, EventArgs e)
        {
            lblAclik.Text = tom.AclikPuanı.ToString();
            lblEglence.Text = tom.EglencePuanı.ToString();
            lblEnerji.Text = tom.EnerjiPuanı.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblAclik.Text = tom2.AclikPuanı.ToString();
            lblEglence.Text = tom2.EglencePuanı.ToString();
            lblEnerji.Text = tom2.EnerjiPuanı.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tom2=new Tom();
        }

        private void btnDoyur_Click(object sender, EventArgs e)
        {
            tom.YemekYe();
        }

        private void btnUyut_Click(object sender, EventArgs e)
        {
            tom.Uyu();
        }

        private void btnEglendir_Click(object sender, EventArgs e)
        {
            tom.OyunOyna();
        }
    }
}
