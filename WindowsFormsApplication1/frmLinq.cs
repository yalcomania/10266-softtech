﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class frmLinq : Form
    {
        public frmLinq()
        {
            InitializeComponent();
        }

        Random r=new Random();
        private void frmLinq_Load(object sender, EventArgs e)
        {
            //dataGridView1.DataSource = GetTumPersoneller();
            

        }

        private List<Personel> GetTumPersoneller()
        {
            List<Personel> tumPersoneller = new List<Personel>();
            for (int i = 0; i < 500; i++)
            {
                Personel p = new Personel();
                p.Ad = Guid.NewGuid().ToString("N").Substring(0, 5).ToUpper();
                p.Soyad = Guid.NewGuid().ToString("N").Substring(0, 8).ToUpper();
                p.DogumTarihi = DateTime.Now.AddDays(r.Next(6570, 14600) * -1);
                p.Maas = r.Next(900, 25000);
                p.Id = i + 1;
                tumPersoneller.Add(p);
            }

            List<Personel> mudurler = new List<Personel>();
            mudurler.Add(tumPersoneller[0]);
            mudurler.Add(tumPersoneller[7]);
            mudurler.Add(tumPersoneller[65]);
            mudurler.Add(tumPersoneller[179]);

            for (int i = 0; i < 500; i++)
            {
                if (i == 0 || i == 7 || i == 65 || i == 179)
                    continue;

                tumPersoneller[i].Mudur = mudurler[r.Next(0, 4)];
            }

            return tumPersoneller;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = GetTumPersoneller();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //foreach (var personel in GetTumPersoneller())
            //{
                
            //}
            var result = GetTumPersoneller()
                .Where(personel => personel.Ad.Contains("A"))
                .ToList();

            dataGridView1.DataSource = result;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var result =
                GetTumPersoneller()
                    .Where(personel => 
                        personel.Ad.Contains("A") 
                        && personel.Soyad.Contains("B"))
                    .ToList();

            dataGridView1.DataSource = result;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var result =
               GetTumPersoneller()
                   .Where(personel =>
                      personel.Maas>=20000)
                   .ToList();

            dataGridView1.DataSource = result;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var result = GetTumPersoneller()
                .Where(p => DateTime.Now.Year - p.DogumTarihi.Year == 35)
                .ToList();

            dataGridView1.DataSource = result;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var result = GetTumPersoneller()
                .Where(p => p.Mudur!=null && p.Mudur.Ad.Contains("A"))
                .ToList();

    //        var result2 = GetTumPersoneller()
    //.Where(p => p.Mudur!=null)
    //.ToList();

    //        var result3 = result2.Where(p => p.Mudur.Ad.Contains("A")).ToList();

            dataGridView1.DataSource = result;
        } 
    }
}
