﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Sms;

namespace WindowsFormsApplication1
{
    public partial class frmSms : Form
    {
        public frmSms()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string hangiProvider = comboBox1.SelectedItem.ToString();
            SmsProviderType smsProviderType = SmsProviderType.JettMesaj;

            int deger = Convert.ToInt32(smsProviderType);

            SmsBase smsBase = null;
            
            //if (hangiProvider == "Posta Güvercini")
            if (smsProviderType == SmsProviderType.PostaGuvercini)
            {
                smsBase = new PostaGuvercini();
            }
            else if (smsProviderType == SmsProviderType.JettMesaj)
            {
                smsBase = new JettMesaj();
            }
            else if (smsProviderType == SmsProviderType.Infobip)
            {
                smsBase = new Infobip();
            }

            smsBase.SmsGonder(textBox1.Text, textBox2.Text);




        }
    }
}
