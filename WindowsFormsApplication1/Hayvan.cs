﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public abstract class Hayvan
    {
        public int AyakSayisi { get; set; }
        public bool UcabilirMi { get; set; }
        public int Boy { get; set; }
        public int Kilo { get; set; }

        public virtual void Uyu()
        {
            int x = 5;
        }
        public void YemekYe() { }
        public abstract void NefesAl();
    }
}
