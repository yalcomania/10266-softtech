﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public abstract class ReportingBase
    {
        public abstract string ReportName { get; }
        public abstract string SqlQuery { get; }

        public virtual string GetConnectionString()
        {
            return "server=.;database=hede;sldkjdslkfjlsdkfjl";
        }

        private SqlConnection _con;

        void OpenConnection()
        {
            _con = new SqlConnection(GetConnectionString());
            _con.Open();
        }

        void CloseConnecton()
        {
            if (_con.State != ConnectionState.Closed)
                _con.Close();
        }

        public DataSet ProcessReport()
        {
            OpenConnection();
            SqlDataAdapter da = new SqlDataAdapter(SqlQuery, _con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            CloseConnecton();
            return ds;
        }



    }
}
