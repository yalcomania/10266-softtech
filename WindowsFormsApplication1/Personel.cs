﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Personel
    {
        public int Id;
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public DateTime DogumTarihi { get; set; }
        public decimal Maas { get; set; }
        public Personel Mudur{ get; set; }
        public Departman Departman { get; set; }

        public string GetFullName()
        {
            return Ad + " " + Soyad;
        }

        void hede()
        {
            GC.SuppressFinalize(this);
        }

        

        public override string ToString()
        {
            return Ad+ " "+Soyad;
        }

        ~Personel()
        {
            
        }
    }
}
