﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace WindowsFormsApplication1
{
    public class Tom:IClosable
    {
        public byte EnerjiPuanı { get; set; }
        public byte AclikPuanı { get; set; }

        //private byte _aclikPuani ;
        //public byte AclikPuanı
        //{
        //    get { return _aclikPuani; }
        //    set
        //    {
        //        _aclikPuani=val
        //        if (value == 30)
        //        {
        //            MessageBox.Show("hede");
        //        }

        //    } 
        //}

        public byte EglencePuanı { get; set; }

        private System.Timers.Timer _timer;

        public Tom()
        {
            EnerjiPuanı = 100;
            AclikPuanı = 100;
            EglencePuanı = 100;
            _timer = new Timer();
            _timer.Interval = 1000; //1 saniye
            _timer.Elapsed += _timer_Elapsed; //_timer_Elapsed metoda git
            _timer.Start();
        }

        public static int YasHesapla(int year)
        {
            
            return DateTime.Now.Year - year;
        }

        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            EnerjiPuanı -= 3;
            AclikPuanı -= 2;
            EglencePuanı -= 1;
        }

        public void YemekYe()
        {
            AclikPuanı = 100;
        }

     
        public void OyunOyna()
        {
            EglencePuanı = 100;
        }

        public void Uyu()
        {
            EnerjiPuanı = 100;
        }


        public void Close()
        {
            
        }
    }
}
