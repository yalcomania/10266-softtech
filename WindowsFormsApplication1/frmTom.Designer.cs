﻿namespace WindowsFormsApplication1
{
    partial class frmTom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblAclik = new System.Windows.Forms.Label();
            this.lblEnerji = new System.Windows.Forms.Label();
            this.lblEglence = new System.Windows.Forms.Label();
            this.btnSonDurum = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDoyur = new System.Windows.Forms.Button();
            this.btnUyut = new System.Windows.Forms.Button();
            this.btnEglendir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Açlık";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(101, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enerji";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(194, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Eğlence";
            // 
            // lblAclik
            // 
            this.lblAclik.AutoSize = true;
            this.lblAclik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAclik.Location = new System.Drawing.Point(12, 38);
            this.lblAclik.Name = "lblAclik";
            this.lblAclik.Size = new System.Drawing.Size(14, 20);
            this.lblAclik.TabIndex = 3;
            this.lblAclik.Text = "-";
            // 
            // lblEnerji
            // 
            this.lblEnerji.AutoSize = true;
            this.lblEnerji.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEnerji.Location = new System.Drawing.Point(100, 38);
            this.lblEnerji.Name = "lblEnerji";
            this.lblEnerji.Size = new System.Drawing.Size(14, 20);
            this.lblEnerji.TabIndex = 3;
            this.lblEnerji.Text = "-";
            // 
            // lblEglence
            // 
            this.lblEglence.AutoSize = true;
            this.lblEglence.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEglence.Location = new System.Drawing.Point(189, 38);
            this.lblEglence.Name = "lblEglence";
            this.lblEglence.Size = new System.Drawing.Size(14, 20);
            this.lblEglence.TabIndex = 3;
            this.lblEglence.Text = "-";
            // 
            // btnSonDurum
            // 
            this.btnSonDurum.Location = new System.Drawing.Point(113, 129);
            this.btnSonDurum.Name = "btnSonDurum";
            this.btnSonDurum.Size = new System.Drawing.Size(168, 30);
            this.btnSonDurum.TabIndex = 4;
            this.btnSonDurum.Text = "Son Durum Nedir?";
            this.btnSonDurum.UseVisualStyleBackColor = true;
            this.btnSonDurum.Click += new System.EventHandler(this.btnSonDurum_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(113, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 30);
            this.button1.TabIndex = 4;
            this.button1.Text = "Son Durum Nedir 2?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 129);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 30);
            this.button2.TabIndex = 5;
            this.button2.Text = "Yeni Tom";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDoyur
            // 
            this.btnDoyur.Location = new System.Drawing.Point(12, 61);
            this.btnDoyur.Name = "btnDoyur";
            this.btnDoyur.Size = new System.Drawing.Size(87, 23);
            this.btnDoyur.TabIndex = 6;
            this.btnDoyur.Text = "Doyur";
            this.btnDoyur.UseVisualStyleBackColor = true;
            this.btnDoyur.Click += new System.EventHandler(this.btnDoyur_Click);
            // 
            // btnUyut
            // 
            this.btnUyut.Location = new System.Drawing.Point(104, 61);
            this.btnUyut.Name = "btnUyut";
            this.btnUyut.Size = new System.Drawing.Size(87, 23);
            this.btnUyut.TabIndex = 7;
            this.btnUyut.Text = "Uyut";
            this.btnUyut.UseVisualStyleBackColor = true;
            this.btnUyut.Click += new System.EventHandler(this.btnUyut_Click);
            // 
            // btnEglendir
            // 
            this.btnEglendir.Location = new System.Drawing.Point(193, 61);
            this.btnEglendir.Name = "btnEglendir";
            this.btnEglendir.Size = new System.Drawing.Size(87, 23);
            this.btnEglendir.TabIndex = 8;
            this.btnEglendir.Text = "Eğlendir";
            this.btnEglendir.UseVisualStyleBackColor = true;
            this.btnEglendir.Click += new System.EventHandler(this.btnEglendir_Click);
            // 
            // frmTom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 208);
            this.Controls.Add(this.btnEglendir);
            this.Controls.Add(this.btnUyut);
            this.Controls.Add(this.btnDoyur);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSonDurum);
            this.Controls.Add(this.lblEglence);
            this.Controls.Add(this.lblEnerji);
            this.Controls.Add(this.lblAclik);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmTom";
            this.Text = "frmTom";
            this.Load += new System.EventHandler(this.frmTom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblAclik;
        private System.Windows.Forms.Label lblEnerji;
        private System.Windows.Forms.Label lblEglence;
        private System.Windows.Forms.Button btnSonDurum;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnDoyur;
        private System.Windows.Forms.Button btnUyut;
        private System.Windows.Forms.Button btnEglendir;
    }
}