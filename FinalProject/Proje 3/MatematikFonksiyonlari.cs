﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Proje_3
{
    public class MatematikFonksiyonlari
    {
        public static decimal Topla(decimal deger1, decimal deger2)
        {
            return deger1 + deger2;
        }

        public static decimal Bol(decimal deger1, decimal deger2)
        {
            return deger1 / deger2;
        }

        public static decimal Carp(decimal deger1, decimal deger2)
        {
            return deger1 * deger2;
        }

        public static decimal Cikar(decimal deger1, decimal deger2)
        {
            return deger1 - deger2;
        }
    }
}
