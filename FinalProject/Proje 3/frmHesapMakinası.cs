﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject.Proje_3
{
    public partial class frmHesapMakinası : Form
    {
        public frmHesapMakinası()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtSonuc1.Text += ((Button) sender).Text;
        }

        private void button23_Click(object sender, EventArgs e)
        {
            try
            {
                var deger = MatematikFonksiyonlari.Topla(Convert.ToDecimal(txtSonuc1.Text), Convert.ToDecimal(txtSonuc2.Text));
                lblSonuc.Text = deger.ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
