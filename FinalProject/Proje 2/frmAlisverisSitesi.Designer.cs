﻿namespace FinalProject
{
    partial class frmAlisverisSitesi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtKrediKarti = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTaksit = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTutar = new System.Windows.Forms.Label();
            this.btnParaCek = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtKrediKarti
            // 
            this.txtKrediKarti.Location = new System.Drawing.Point(91, 25);
            this.txtKrediKarti.Name = "txtKrediKarti";
            this.txtKrediKarti.Size = new System.Drawing.Size(211, 20);
            this.txtKrediKarti.TabIndex = 0;
            this.txtKrediKarti.Text = "4183423412345678";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kredi Kartı";
            // 
            // cmbTaksit
            // 
            this.cmbTaksit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTaksit.FormattingEnabled = true;
            this.cmbTaksit.Items.AddRange(new object[] {
            "Peşin",
            "2",
            "3",
            "6",
            "12"});
            this.cmbTaksit.Location = new System.Drawing.Point(91, 62);
            this.cmbTaksit.Name = "cmbTaksit";
            this.cmbTaksit.Size = new System.Drawing.Size(121, 21);
            this.cmbTaksit.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Taksit";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tutar";
            // 
            // lblTutar
            // 
            this.lblTutar.AutoSize = true;
            this.lblTutar.Location = new System.Drawing.Point(91, 102);
            this.lblTutar.Name = "lblTutar";
            this.lblTutar.Size = new System.Drawing.Size(40, 13);
            this.lblTutar.TabIndex = 5;
            this.lblTutar.Text = "216.70";
            // 
            // btnParaCek
            // 
            this.btnParaCek.Location = new System.Drawing.Point(189, 141);
            this.btnParaCek.Name = "btnParaCek";
            this.btnParaCek.Size = new System.Drawing.Size(113, 23);
            this.btnParaCek.TabIndex = 6;
            this.btnParaCek.Text = "Para Çek";
            this.btnParaCek.UseVisualStyleBackColor = true;
            this.btnParaCek.Click += new System.EventHandler(this.btnParaCek_Click);
            // 
            // frmAlisverisSitesi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 190);
            this.Controls.Add(this.btnParaCek);
            this.Controls.Add(this.lblTutar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbTaksit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKrediKarti);
            this.Name = "frmAlisverisSitesi";
            this.Text = "Hepsi Nerede?";
            this.Load += new System.EventHandler(this.frmAlisverisSitesi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKrediKarti;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTaksit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTutar;
        private System.Windows.Forms.Button btnParaCek;
    }
}