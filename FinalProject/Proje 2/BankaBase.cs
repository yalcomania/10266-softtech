﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public abstract class BankaBase
    {
        public abstract void ParaCek(string kkNo, int taksit, decimal tutar);
    }
}
