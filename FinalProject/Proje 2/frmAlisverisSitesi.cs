﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinalProject.Proje_2;

namespace FinalProject
{
    public partial class frmAlisverisSitesi : Form
    {
        public frmAlisverisSitesi()
        {
            InitializeComponent();
        }

        private void btnParaCek_Click(object sender, EventArgs e)
        {
            string banka = "";
            string ilk6 = txtKrediKarti.Text.Substring(0, 6);
            foreach (var bin in _bin)
            {
                if (bin.BinNo == ilk6)
                {
                    banka = bin.Banka;
                    break;
                }
            }

            BankaBase bankaBase = null;
            if (banka == "İş")
            {
                bankaBase=new IsBankasi();
            }
            else if (banka == "YKB")
            {
                bankaBase=new YKB();
            }
            else
            {
                MessageBox.Show("Bilinmeyen Kart");
                return;
            }

            int taksit = 0;
            if (cmbTaksit.SelectedIndex > 0)
            {
                taksit = Convert.ToInt32(cmbTaksit.SelectedItem);
            }
            bankaBase.ParaCek(txtKrediKarti.Text,taksit,Convert.ToDecimal(lblTutar.Text));

        }

        private void ParaCek(string kkNo, int taksit, decimal tutar)
        {
            
        }

        List<Bin> _bin=new List<Bin>();
 
        private void frmAlisverisSitesi_Load(object sender, EventArgs e)
        {
            Bin bin=new Bin();
            bin.Banka = "İş";
            bin.BinNo = "418342";

            Bin bin2 = new Bin();
            bin2.Banka = "YKB";
            bin2.BinNo = "450634";

            _bin.Add(bin);
            _bin.Add(bin2);

        }
    }
}
