﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject
{
    public class IsBankasi:BankaBase
    {
        public override void ParaCek(string kkNo, int taksit, decimal tutar)
        {
            MessageBox.Show("İşbankası tarafından çekildi.\r\nTaksit:" + taksit + "\r\n" + tutar.ToString("c"));
        }
    }
}
