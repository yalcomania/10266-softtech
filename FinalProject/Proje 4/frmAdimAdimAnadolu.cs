﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject.Proje_4
{
    public partial class frmAdimAdimAnadolu : Form
    {
        public frmAdimAdimAnadolu()
        {
            InitializeComponent();
        }

        private void frmAdimAdimAnadolu_Load(object sender, EventArgs e)
        {
            foreach (var il in TumIlleriGetir())
            {
                comboBox1.Items.Add(il);
            }
        }

        List<Il> TumIlleriGetir()
        {
            List<Il> iller=new List<Il>();
            
            Il il=new Il();
            il.Ad = "İstanbul";
            il.Id = 34;
            il.Nufus = 14000000;

            il.Ilceler=new List<Ilce>();
            il.Ilceler.Add(new Ilce() { Id = 1, Ad = "Beşiktaş" });
            il.Ilceler.Add(new Ilce() { Id = 2, Ad = "Kadıköy" });
            il.Ilceler.Add(new Ilce() { Id = 3, Ad = "Beyoğlu" });
            il.Ilceler.Add(new Ilce() { Id = 4, Ad = "Eminönü" });
            il.Ilceler.Add(new Ilce() { Id = 5, Ad = "Ataşehir" });

            Il il2 = new Il();
            il2.Ad = "Denizli";
            il2.Id = 20;
            il2.Nufus = 978700;

            il2.Ilceler = new List<Ilce>();
            il2.Ilceler.Add(new Ilce() { Id = 11, Ad = "Merkez" });
            il2.Ilceler.Add(new Ilce() { Id = 12, Ad = "Serinhisar" });
            il2.Ilceler.Add(new Ilce() { Id = 13, Ad = "Tavas" });
            il2.Ilceler.Add(new Ilce() { Id = 14, Ad = "Kale" });
            il2.Ilceler.Add(new Ilce() { Id = 15, Ad = "Sarayköy" });

            Il il3 = new Il();
            il3.Ad = "İzmir";
            il3.Id = 35;
            il3.Nufus = 4113072;

            il3.Ilceler = new List<Ilce>();
            il3.Ilceler.Add(new Ilce() { Id = 21, Ad = "Bornova" });
            il3.Ilceler.Add(new Ilce() { Id = 22, Ad = "Gaziemir" });
            il3.Ilceler.Add(new Ilce() { Id = 23, Ad = "Kemalpaşa" });
            il3.Ilceler.Add(new Ilce() { Id = 24, Ad = "Tire" });
            il3.Ilceler.Add(new Ilce() { Id = 25, Ad = "Urla" });

            iller.Add(il);
            iller.Add(il2);
            iller.Add(il3);

            return iller;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            var il = (Il) comboBox1.SelectedItem;
            lblNufus.Text = il.Nufus.ToString();
            foreach (var ilce in il.Ilceler)
            {
                listBox1.Items.Add(ilce);
            }
            
        } 
    }
}
