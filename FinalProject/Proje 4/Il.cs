﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Proje_4
{
    public class Il
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public int Nufus { get; set; }
        public List<Ilce> Ilceler { get; set; }

        public override string ToString()
        {
            return Ad;
        }
    }
}
