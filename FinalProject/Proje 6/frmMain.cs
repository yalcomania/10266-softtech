﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject.Proje_6
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmForm1 f=new frmForm1();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmForm2 f = new frmForm2();
            f.Show();
            
            
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ProjeDegiskenleri.LoginTime = DateTime.Now;
        }
    }
}
