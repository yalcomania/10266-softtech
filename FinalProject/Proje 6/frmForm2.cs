﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject.Proje_6
{
    public partial class frmForm2 : Form
    {
        public frmForm2()
        {
            InitializeComponent();
        }

        private void frmForm2_Load(object sender, EventArgs e)
        {
            double kacSaniyeGecti = DateTime.Now.Subtract(ProjeDegiskenleri.LoginTime).TotalSeconds;
            lblSonLogin.Text = kacSaniyeGecti.ToString();
        }
    }
}
