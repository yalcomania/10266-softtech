﻿namespace FinalProject.Proje_5
{
    partial class frmMaxBoyut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDisket = new System.Windows.Forms.Button();
            this.btnCD = new System.Windows.Forms.Button();
            this.btnDVD = new System.Windows.Forms.Button();
            this.btnBlueray = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDisket
            // 
            this.btnDisket.Location = new System.Drawing.Point(32, 13);
            this.btnDisket.Name = "btnDisket";
            this.btnDisket.Size = new System.Drawing.Size(170, 23);
            this.btnDisket.TabIndex = 0;
            this.btnDisket.Text = "Disket";
            this.btnDisket.UseVisualStyleBackColor = true;
            this.btnDisket.Click += new System.EventHandler(this.btnDisket_Click);
            // 
            // btnCD
            // 
            this.btnCD.Location = new System.Drawing.Point(32, 42);
            this.btnCD.Name = "btnCD";
            this.btnCD.Size = new System.Drawing.Size(170, 23);
            this.btnCD.TabIndex = 1;
            this.btnCD.Text = "CD";
            this.btnCD.UseVisualStyleBackColor = true;
            // 
            // btnDVD
            // 
            this.btnDVD.Location = new System.Drawing.Point(32, 71);
            this.btnDVD.Name = "btnDVD";
            this.btnDVD.Size = new System.Drawing.Size(170, 23);
            this.btnDVD.TabIndex = 2;
            this.btnDVD.Text = "DVD";
            this.btnDVD.UseVisualStyleBackColor = true;
            // 
            // btnBlueray
            // 
            this.btnBlueray.Location = new System.Drawing.Point(32, 100);
            this.btnBlueray.Name = "btnBlueray";
            this.btnBlueray.Size = new System.Drawing.Size(170, 23);
            this.btnBlueray.TabIndex = 3;
            this.btnBlueray.Text = "Blueray";
            this.btnBlueray.UseVisualStyleBackColor = true;
            // 
            // frmMaxBoyut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 136);
            this.Controls.Add(this.btnBlueray);
            this.Controls.Add(this.btnDVD);
            this.Controls.Add(this.btnCD);
            this.Controls.Add(this.btnDisket);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMaxBoyut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMaxBoyut";
            this.Load += new System.EventHandler(this.frmMaxBoyut_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDisket;
        private System.Windows.Forms.Button btnCD;
        private System.Windows.Forms.Button btnDVD;
        private System.Windows.Forms.Button btnBlueray;
    }
}